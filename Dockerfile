FROM node:current-alpine3.17
WORKDIR /src
COPY . .
EXPOSE 3000
CMD [ "node",  "node.js" ]